// Import signale
import * as signale from 'signale';

export function log (string) {
    signale.log(string)
}