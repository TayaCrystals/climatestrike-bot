// Import discord.js
import Discord from 'discord.js';

// Create an instance of a Discord client
const client = new Discord.Client();

client.on('ready', () => {
  
});

// Create an event listener for messages
client.on('message', message => {
  // If the message is "ping"
  if (message.content === 'ping') {
    // Send "pong" to the same channel
    message.channel.send('pong');
  }
});

// Log our bot in using the token from https://discordapp.com/developers/applications/me
client.login('your token here');