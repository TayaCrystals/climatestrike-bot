# Goal

Provide a telegram and discord bot with api, command and web interface control

# Structure

Discord and telegram modules are here to communicate with the respective apis, the handling of messages and actions in done by a central module

# Tech

Environment: [Node.js](nodejs.org)  
Language: [Typescript](https://www.typescriptlang.org/)  
Process Manager: [pm2](https://pm2.io/)  
Package Manager: [pnpm](https://pnpm.js.org/)  
Libraries:  
- Discord: [discord.js](https://discord.js.org/)  
- Telgram: [telegraf.js](https://telegraf.js.org/)  
- Logger: [signale](https://github.com/klaussinani/signale)  